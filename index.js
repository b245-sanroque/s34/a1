/*
	- Create a GET route that will access the /home route that will print out a simple message.
	- Process a GET request at the /home route using postman.
	- Create a GET route that will access the /items route that will retrieve all the users in the mock database.
	- Process a GET request at the /items route using postman.
	- Create a DELETE route that will access the /delete-item route to remove a user from the mock database.
	- Process a DELETE request at the /delete-item route using postman.
*/

const express = require("express");
const port = 8080;
const app = express();

// app.use
app.use(express.json());


let items = [
    {
        name: "Mjolnir",
        price: 50000,
        isActive: true
    },
    {
        name: "Vibranium Shield",
        price: 70000,
        isActive: true
    }
];


// /home
app.get("/home", (request, response) => {
		response.send("You are currently in our Home page!")
	})


// /items
app.get("/items", (request, response) => {
		response.send(items)
	})


// /delete
app.delete("/items", (request, response) => {
		response.send(items.pop());
	})

//app.listen
app.listen(port, ()=>console.log(`server is running at port ${port}`));